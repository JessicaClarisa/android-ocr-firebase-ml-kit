package com.example.ocrmlkit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.otaliastudios.cameraview.Audio;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;

public class MainActivity extends AppCompatActivity {

    CameraView cameraView;
    ImageView imageView;
    RelativeLayout overlayCamera;
    ScrollView overlayResult;
    TextView result;
    Button btnTakePicture;
    Bitmap bitmapPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cameraView = (CameraView) findViewById(R.id.camera_view);
        imageView = (ImageView) findViewById(R.id.image_view);
        overlayCamera = (RelativeLayout) findViewById(R.id.relative_layout_panel_overlay_camera);
        overlayResult = (ScrollView) findViewById(R.id.scrollView);
        result = (TextView) findViewById(R.id.text_view_result);
        btnTakePicture = (Button) findViewById(R.id.button_take_picture);

        initCameraView();
        initListeners();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
        }

    }

    private void initCameraView() {
        cameraView.setAudio(Audio.OFF);
        cameraView.setPlaySounds(false);
        cameraView.setCropOutput(true);
    }

    private void initListeners() {
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                cameraView.stop();
                CameraUtils.decodeBitmap(jpeg, new CameraUtils.BitmapCallback() {
                    @Override
                    public void onBitmapReady(Bitmap bitmap) {
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageView.setImageBitmap(bitmap);
                        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
                        FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance()
                                .getOnDeviceTextRecognizer();
                        textRecognizer.processImage(image)
                                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                                    @Override
                                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                        cameraView.setVisibility(View.GONE);
                                        imageView.setVisibility(View.VISIBLE);
                                        overlayCamera.setVisibility(View.GONE);
                                        overlayResult.setVisibility(View.VISIBLE);
                                        processTextRecognitionResult(firebaseVisionText);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                super.onPictureTaken(jpeg);
            }
        });

        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Ini untuk gambar dengan resolusi tinggi
                //cameraView.capturePicture();

                // Ini untuk gambar dengan resolusi yang lebih rendah
                cameraView.captureSnapshot();
            }
        });
    }

    private void processTextRecognitionResult(FirebaseVisionText firebaseVisionText) {
        result.setText(firebaseVisionText.getText());
    }

    private void showCameraView() {
        cameraView.start();
        cameraView.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        overlayCamera.setVisibility(View.VISIBLE);
        overlayResult.setVisibility(View.GONE);
    }

    private void useExistingImage(){
        if(cameraView.isStarted())
            cameraView.stop();
        cameraView.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
        overlayCamera.setVisibility(View.GONE);
        overlayResult.setVisibility(View.GONE);
    }

    private void processExistingImage() {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmapPicture);
        FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        textRecognizer.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                        cameraView.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);
                        overlayCamera.setVisibility(View.GONE);
                        overlayResult.setVisibility(View.VISIBLE);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        processTextRecognitionResult(firebaseVisionText);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showKTP(String gender) {
        useExistingImage();
        if(gender.equals("pria"))
            bitmapPicture = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ocr_sample_pria);
        else if (gender.equals("wanita"))
            bitmapPicture = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ocr_sample_wanita);

        imageView.setImageBitmap(bitmapPicture);
        imageView.setVisibility(View.VISIBLE);
        processExistingImage();
    }


    @Override
    protected void onPause() {
        if(cameraView.isStarted()) {
            cameraView.stop();
        }
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_camera:
                showCameraView();
                break;
            case R.id.menu_item_ktp_pria:
                showKTP("pria");
                break;
            case R.id.menu_item_ktp_wanita:
                showKTP("wanita");
                break;
            default:
                return false;
        }
        return true;
    }
}